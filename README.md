# switchhb_vm
VM image containing preconfigured Switch homebrew utilities, for both development and usage.

switchhb_vm uses Ubuntu 16.04 LTS for the operating system, and all that is added is the utilities below. This means you can do anything on switchhb_vm that you can do on a regular Ubuntu 16.04 virtual machine!

## Utilities included (in no particular order)
- Node.js & npm
- Python & pip (both 2.7 and 3.6)
- clang/LLVM
- git
- PegaSwitch
- libtransistor
- libnx
- nx-hbexploit300
- switch-tools
- devkitPro/devkitPPC/devkitARM/devkitA64
- A custom script by me to easily make new libnx homebrew applications from templates
- Mephisto
- CageTheUnicorn
- nxdbg
- RyujiNX

Most of the utilities above that require them to be compiled manually have been precompiled in this VM image, so for the most part you don't have to do any compilation of source code! (Unless the repos get updated, then you need to git pull the new repos, and follow their steps to compile - I plan on adding a bash script to make this process easier)

## Installation
Make sure you have [Vagrant](https://vagrantup.com) installed, and a hypervisor (I strongly recommend VMware Workstation)

Run the following commands in your terminal:
```
$ vagrant box add CuriosityDev/switchhb_vm
```
Then Vagrant will setup the VM. Then you can use the following commands.

**To SSH into the VM**:
```
$ vagrant ssh <vm name>
```
**To gain access to a desktop environment (GUI)**:
```
$ vagrant up <vm name>
```
and once it loads you can run
```
$ sudo service lightdm start
```
or
```
$ sudo startx
```
to get the Ubuntu login screen and login with the credentials below.

**Note**: I will __*not*__ be providing support for loading the files from the download into your hypervisor, unless the problem ends up being on my end (either a packaging issue, or I didn't export the right filetype, or the file I exported was broken from the start, etc).

## Logon information
- Username: `switch_brew`
- Password: `switchbrew`

The above information can be used to logon to the VM, and when using sudo, the password above can be used.
